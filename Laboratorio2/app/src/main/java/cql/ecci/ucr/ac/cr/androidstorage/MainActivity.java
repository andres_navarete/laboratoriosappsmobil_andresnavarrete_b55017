package cql.ecci.ucr.ac.cr.androidstorage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instanciando los botones del layout activity_main.xml
        Button buttonDataBaseInsert = (Button) findViewById(R.id.buttonDataBaseInsert);
        Button buttonDataBaseDelete = (Button) findViewById(R.id.buttonDataBaseDelete);
        Button buttonDataBaseSelect = (Button) findViewById(R.id.buttonDataBaseSelect);
        Button buttonDataBaseUpdate = (Button) findViewById(R.id.buttonDataBaseUpdate);

        // Asocia los eventos click a cada uno de lo botones
        buttonDataBaseSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                leerEstudiante();
            }
        });
        buttonDataBaseDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                eliminarEstudiante();
            }
        });
        buttonDataBaseUpdate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                actualizarEstudiante();
            }
        });
        buttonDataBaseInsert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                insertarEstudiante();
            }
        });

        // almacenamiento de archivos internos
        Button buttonGrabarArchivo = (Button) findViewById(R.id.buttonGrabarArchivo);
        Button buttonLeerArchivo = (Button) findViewById(R.id.buttonLeerArchivo);

        buttonGrabarArchivo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                grabarArchivo();
            }
        });

        buttonLeerArchivo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                leerArchivo();
            }
        });
    }

    // grabar un archivo en almacenamiento interno
    public void grabarArchivo() {
        ParsePosition pos = new ParsePosition(0);
        // crear la persona
        Persona persona = new Persona(
                "1-1000-1000",
                "estudiante01@ucr.ac.cr",
                "Juan",
                "Perez",
                "Soto*",
                "2511-0000",
                "8890-0000",
                new SimpleDateFormat("DD-MM-YYYY").parse("01-01-1995", pos),
                Persona.TIPO_ESTUDIANTE,
                Persona.GENERO_MASCULINO
        );
        // grabar la persona en un archivo interno
        UtilFiles.guardarArchivoInterno(getApplicationContext(), "PersonaAndroidStorage.json", persona.toJson());
        // mensaje al usuario
        Toast.makeText(getApplicationContext(), "Archivo creado: " + "PersonaAndroidStorage.json", Toast.LENGTH_LONG).show();
    }

    // leer un archivo en almacenamiento interno
    public void leerArchivo() {

        // leer el archivo
        String datosArchivo = datosArchivo = UtilFiles.leerArchivoInterno(getApplicationContext(), "PersonaAndroidStorage.json");

        if (datosArchivo.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No hay datos para: " + "PersonaAndroidStorage.json", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Archivo: " + "PersonaAndroidStorage.json" + "Contenido: " + datosArchivo, Toast.LENGTH_LONG).show();
        }

    }

    private void insertarEstudiante() {
        ParsePosition pos = new ParsePosition(0);
        // Instancia la clase Estudiante y realiza la inserción de datos
        Estudiante estudiante = new Estudiante("1-1000-1000",
                "estudiante01@ucr.ac.cr",
                "Juan",
                "Perez",
                "Soto",
                "2511-0000",
                "8890-0000",
                new SimpleDateFormat("DD-MM-YYYY").parse("01-01-1995", pos),
                Persona.TIPO_ESTUDIANTE,
                Persona.GENERO_MASCULINO,
                "A99148",
                1,
                8.0);

        // inserta el estudiante, se le pasa como parametro el contexto de la app
        long newRowId = estudiante.insertar(getApplicationContext());

        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(),
                "Insertar Estudiante: " + newRowId +
                        " Id: " + estudiante.getIdentificacion() +
                        " Carnet: " + estudiante.getCarnet() +
                        " Nombre: " + estudiante.getNombre() +
                        " " + estudiante.getPrimerApellido() +
                        " " + estudiante.getSegundoApellido() +
                        " Correo: " + estudiante.getCorreo() +
                        " Tipo: " + estudiante.getTipo() +
                        " Promedio " + estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
    }

    private void leerEstudiante() {

        // Instancia la clase Estudiante y realiza la lectura de datos
        Estudiante estudiante = new Estudiante();

        // leer el estudiante, se le pasa como parametro el contexto de la app y ls identificacion
        estudiante.leer(getApplicationContext(), "1-1000-1000");

        // si lee al estudiante
        if (estudiante.getTipo().equals(Persona.TIPO_ESTUDIANTE)) {
            // Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(),
                    "Leer Estudiante: " + estudiante.getIdentificacion()
                            + " Carnet: " + estudiante.getCarnet()
                            + " Nombre: " + estudiante.getNombre()
                            + " " + estudiante.getPrimerApellido()
                            + " " + estudiante.getSegundoApellido()
                            + " Correo: " + estudiante.getCorreo()
                            + " Tipo: " + estudiante.getTipo()
                            + " Promedio: " + estudiante.getPromedioPonderado(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "No hay datos para: " + "1-1000-1000", Toast.LENGTH_LONG).show();
        }
    }

    private void eliminarEstudiante() {
        // Primero la instanciacion del estudiante y la realizacion del borrado
        Estudiante estudiante = new Estudiante();

        // Lectura del estudiante que se le pasa como parametro
        estudiante.eliminar(getApplicationContext(), "1-1000-1000");

        // Mostrar un mensaje para el usuario
        Toast.makeText(getApplicationContext(), "Eliminar Estudiante", Toast.LENGTH_LONG).show();
    }

    private void actualizarEstudiante() {
        // Instanciacion del estudiante para la posterior actualizacion de los datos
        ParsePosition pos = new ParsePosition(0);
        Estudiante estudiante = new Estudiante(
                "1-1000-1000",
                "estudiante01@ucr.ac.cr*",
                "Juan*",
                "Perez*",
                "Soto*",
                "2511-0000*",
                "8890-0000*",
                new SimpleDateFormat("DD-MM-YYYY").parse("01-01-1995*", pos),
                Persona.TIPO_ESTUDIANTE,
                Persona.GENERO_MASCULINO,
                "A99148*",
                1,
                8.0);

        // actualiza el estudiante, se le pasa como parametro el contexto de la app
        int contador = estudiante.actualizar(getApplicationContext());

        // Si actualiza el estudiante
        if (contador > 0) {
            // Mostrar un mensaje para el usuario
            Toast.makeText(getApplicationContext(),
                    "Actualizar Estudiante: " +
                            contador +
                            " Id: " + estudiante.getIdentificacion() +
                            " Carnet: " + estudiante.getCarnet() +
                            " Nombre: " + estudiante.getNombre() +
                            " " + estudiante.getPrimerApellido() +
                            " " + estudiante.getSegundoApellido() +
                            " Correo: " + estudiante.getCorreo() +
                            " Tipo: " + estudiante.getTipo() +
                            " Promedio: " + estudiante.getPromedioPonderado(),
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "No hay datos para: " +
                    estudiante.getIdentificacion(), Toast.LENGTH_LONG).show();
        }
    }
}
